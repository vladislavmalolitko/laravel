<?php

use App\Http\Controllers\MovieController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware(['auth'])->group(function () {
    Route::get('/', [MovieController::class, 'showMovies']);
    Route::get('/show/{id}', [MovieController::class, 'show'])->name('show');
    Route::get('/category/{id}', [MovieController::class, 'showMoviesByCategory']);
    Route::get('/search', function () {
        return view('pages.search_movie');
    });
    Route::get('/preview', [MovieController::class, 'checkMovieInDb']);
    Route::get('/edit/{id}', [MovieController::class, 'editMovie']);
    Route::get('/delete/{id}', [MovieController::class, 'deleteMovie']);

    Route::get('/autocomplete', [MovieController::class, 'autocompleteInDb']);
    Route::get('/searchInIMDB', [MovieController::class, 'searchMovie']);

    Route::post('/checkAction', [MovieController::class, 'checkAction'])->name('checkAction');
    Route::post('/updateData/{id}', [MovieController::class, 'editAddedMovieInfo'])->name('updateData');
    Route::post('/createEditedData', [MovieController::class, 'editNoAddedMovieInfo'])->name('createEditedData');

    Route::post('/refreshData', [MovieController::class, 'refreshData'])->name('refreshData');

});

require __DIR__ . '/auth.php';
