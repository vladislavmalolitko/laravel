/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!**************************************!*\
  !*** ./resources/js/autocomplete.js ***!
  \**************************************/
$(document).ready(function () {
  var features = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.whitespace('search_query'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    limit: 20,
    remote: {
      url: '/autocomplete?keyword=%QUERY%',
      wildcard: '%QUERY%'
    }
  });
  features.initialize();
  $('#search_field').typeahead({
    hint: true,
    highlight: true,
    minLength: 2
  }, {
    name: 'features',
    displayKey: 'title',
    limit: 10,
    source: features.ttAdapter(),
    templates: {
      empty: ['<div class="empty-message">', 'Unable to find any movies that match the current query', '</div>'].join('\n'),
      suggestion: Handlebars.compile('<ul class="ui-menu ui-widget ui-widget-content ui-corner-all' + ' ui-front"><li class="ui-menu-item"' + ' onmouseover="this.style.backgroundColor=\'#8893f7\';"' + ' onmouseout="this.style.backgroundColor=\'#FFFFFF\';"><a class="ui-menu-item-wrapper"' + ' href="/show/{{id}}">{{origin_title}}</a></li></ul>')
    }
  });
});
$(document).ready(function () {
  var features = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.whitespace('search_query'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    limit: 20,
    remote: {
      url: '/searchInIMDB?keyword=%QUERY%',
      wildcard: '%QUERY%'
    }
  });
  features.initialize();
  $('.typeaheadDiv #searchInIMDB').typeahead({
    hint: true,
    highlight: true,
    minLength: 2
  }, {
    name: 'features',
    displayKey: 'title',
    limit: 10,
    source: features.ttAdapter(),
    templates: {
      empty: ['<div class="empty-message">', 'Unable to find any movies that match the current query', '</div>'].join('\n'),
      suggestion: Handlebars.compile('<div class="divContainer container">' + '<div class="divItem row justify-content-md-center mx-auto">' + '<span class="col-md-5"><img src="{{img}}" class="small-thumb"></span>' + '<span class="col-md-7" name="nameMovie"><a class="nav-link"' + ' href="preview?link={{link}}">{{title}}</a></span>' + '</div></div>')
    }
  });
});
/******/ })()
;