/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!***************************************!*\
  !*** ./resources/js/movieEditInfo.js ***!
  \***************************************/
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
$(document).ready(function () {
  $('.refresh').click(function () {
    var name = $(this).attr('name');
    $.ajax({
      url: "/refreshData",
      data: {
        "name": name,
        "link": $(this).val()
      },
      dataType: "json",
      type: "post",
      success: function success(data) {
        if (name === 'storyline') {
          $("textarea[name='" + name + "']").val(data);
        }

        if (name === 'img') {
          $("img[id='" + name + "']").attr("src", data);
          $("input[name='img']").val(data['img']);
        }

        if (name === 'category') {
          $("select[name='category_id']").val(data);
        }

        if (name === 'refreshAll') {
          $("input[name='title']").val(data['title']);
          $("input[name='year']").val(data['year']);
          $("input[name='origin_title']").val(data['origin_title']);
          $("input[name='runtime']").val(data['runtime']);
          $("input[name='rating']").val(data['rating']);
          $("textarea[name='storyline']").val(data['storyline']);
          $("select[name='category_id']").val(data['category_id']);
          $("img[id='img']").attr("src", data['img']);
          $("input[id='img']").val(data['img']);
        }

        $("input[name='" + name + "']").val(data);
      }
    });
  });
});

function closeAlert() {
  $('.alert').hide();
}

setTimeout(closeAlert, 3000);
/******/ })()
;