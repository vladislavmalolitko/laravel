@extends('layouts.main')

@section('title', 'Info page movie')

@section('content')
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-secondary">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <!-- HOME-->
                    <li class="nav-item active">
                        <a class="navbar-brand" href="/">LocalHomeMovieDB <span
                                class="sr-only">(current)</span></a>
                    </li>
                    <!-- END HOME-->
                    <li class="nav-item">
                        <a class="nav-link " href="/search">Search movie</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="">My profile</a>
                    </li>
                </ul>
                <div class="form-inline my-2 my-lg-0">
                    <div class="btn-group mr-2" role="group">
                        <form method="POST" action="{{ route('logout') }}">
                            @csrf
                            <button type="submit" name="logout" class="btn btn-danger btn-block"> Log out</button>
                        </form>
                    </div>

                </div>

            </div>
        </nav>
    </header>

    <main role="main">
        @switch ($status)
            @case('addedMovieInfo')
            @include('formsInfo.addedMovieInfo')
            @break
            @case('noAddedMovieInfo')
            @include('formsInfo.noAddedMovieInfo')
            @break
            @case('editAddedMovieInfo')
            @include('formsInfo.editAddedMovieInfo')
            @break
        @endswitch
    </main>
@endsection
