@extends('layouts.main')

@section('title', 'Home page')

@section('content')
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-secondary">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <!-- HOME-->
                    <li class="nav-item active">
                        <a class="navbar-brand" href="/">LocalHomeMovieDB <span
                                class="sr-only">(current)</span></a>
                    </li>
                    <!-- END HOME-->
                    <li class="nav-item">
                        <a class="nav-link " href="/search">Search movie</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="">My profile</a>
                    </li>

                    <!-- Single button -->
                    <li class="nav-item">
                        <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle nav-link" type="button" id="dropdownMenu1"
                                    data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="true">
                                Categories
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <li><a class="nav-link" href="/">All</a></li>
                                @if(!empty($categories))
                                    @foreach($categories as $category)
                                        <li><a class="nav-link"
                                               href="/category/{{$category->id}}">{{$category->name}}</a></li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </li>

                </ul>
                <div class="form-inline my-2 my-lg-0">

                    <input class="form-control mr-sm-2" name="searchInDb" id="search_field" type="text"
                           placeholder="Search"
                           aria-label="Search" autocomplete="off">
                    <span id="search_result"></span>

                    <div class="btn-group mr-2" role="group">
                        <form method="POST" action="{{ route('logout') }}">
                            @csrf
                            <button type="submit" name="logout" class="btn btn-danger btn-block"> Log out</button>
                        </form>
                    </div>

                </div>

            </div>
        </nav>
    </header>
    <main role="main">
        <div class="container">
            @if($movies->isEmpty())
                <div class="row justify-content-center">
                    <h1>No movies added</h1>
                </div>
            @else
                <div class="album py-5 bg-light">
                    <div class="row">

                        @foreach ($movies as $movie)
                            <div class="col-xl-3 col-md-4">
                                <div class="card mb-4 box-shadow">
                                    {{--                                    @dd($movie->img)--}}
                                    <img class="film-img"
                                         src="{{$movie->img}}"
                                         alt="{{$movie->title}}">
                                    <div class="card-body">
                                        <p class="card-text">{{$movie->title}}</p>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="col text-center">
                                                <div class="btn-group">
                                                    <a href="/show/{{$movie->id}}"
                                                       class="btn btn-sm btn-outline-success">
                                                        View
                                                    </a>
                                                    <a href="/delete/{{$movie->id}}"
                                                       class="btn btn-sm btn-outline-danger">
                                                        Delete
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="d-flex justify-content-center align-items-end">
                        {{ $movies->onEachSide(5)->links() }}
                    </div>
                </div>
            @endif
        </div>
    </main>
@endsection
