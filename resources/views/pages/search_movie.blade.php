@extends('layouts.main')

@section('title', 'Home page')

@section('content')
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-secondary">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <!-- HOME-->
                    <li class="nav-item active">
                        <a class="navbar-brand" href="/">LocalHomeMovieDB <span
                                class="sr-only">(current)</span></a>
                    </li>
                    <!-- END HOME-->
                    <li class="nav-item active">
                        <a class="nav-link " href="/search">Search movie</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="">My profile</a>
                    </li>
                </ul>
                <div class="form-inline my-2 my-lg-0">
                    <div class="btn-group mr-2" role="group">

                        <form method="POST" action="{{ route('logout') }}">
                            @csrf
                            <button type="submit" name="logout" class="btn btn-danger btn-block"> Log out</button>
                        </form>
                    </div>

                </div>

            </div>
        </nav>
    </header>
    <main role="main">
<form>
            <h5 class="card-title mt-3 text-center">Enter the movie you want to watch</h5>
            <article class="card-body mx-auto" style="max-width: 400px;">
                <div class="typeaheadDiv form-group input-group justify-content-md-center mx-auto">
                    <input type="text" class="typeahead form-control" id="searchInIMDB"
                           placeholder="Enter movie"
                           name="title" autocomplete="off">
                </div>
            </article>
</form>
    </main>
@endsection
