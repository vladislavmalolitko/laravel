<form action="checkAction" method="post" enctype="multipart/form-data">
    @csrf
    @if($movie)
        <div class="card bg-light align-items-start">
            <article class="card-body mx-auto" style="max-width: 1000px;">
                <div class="row">

                    <div class="col-12 text-center">
                        <h1 class="display-4">{{$movie['title']}} ({{$movie['year']}})</h1><br>
                        <input type="hidden" name="title" value="{{$movie['title']}}"/>
                        <input type="hidden" name="year" value="{{$movie['year']}}"/>
                    </div>

                    <div class="col-5">
                        <div class="row ">
                            <div class="col text-center">
                                <img id="img" src="{{$movie['img']}}" alt="{{$movie['img']}}">
                                <input type="hidden" class="imgName" name="img" value="{{$movie['img']}}"/>
                            </div>
                        </div>
                    </div>

                    <div class="col-7">
                        <div class="form-group" style="max-width: 500px;">
                            <h5>Origin title</h5>
                            <p>{{$movie['origin_title']}}</p>
                            <input type="hidden" name="origin_title" value="{{$movie['origin_title']}}"/>

                            <h5>Category:</h5>
                            @foreach ($categories as $category)
                                @if ($category['id'] == $movie['category_id'])
                                    <p> {{$category['name']}}</p>
                                @endif
                            @endforeach
                            <input type="hidden" name="category_id" value="{{$movie['category_id']}}"/>

                            <h5>Rating: </h5>
                            <p>{{$movie['rating']}}</p>
                            <input type="hidden" name="rating" value="{{$movie['rating']}}"/>

                            <h5>Runtime: </h5>
                            <p>{{$movie['runtime']}}</p>
                            <input type="hidden" name="runtime" value="{{$movie['runtime']}}"/>

                            <h5>Storyline: </h5>
                            <p>{{$movie['storyline']}}</p>
                            <input type="hidden" name="storyline" value="{{$movie['storyline']}}"/>
                        </div>
                    </div>


                </div>
            </article>

            <div class="form-group container text-center">
                <input type="hidden" name="user_id" value="{{$user_id}}"/>
                <input type="hidden" name="href" value="{{$link}}"/>
                <button type="submit" name="onkey" value="add" class="btn btn-success">Add</button>
            </div>
        </div>


    @else
        <div class="container">
            <div class="row justify-content-center">
                <h1>No info about movie</h1>
            </div>
        </div>
    @endif
</form>
