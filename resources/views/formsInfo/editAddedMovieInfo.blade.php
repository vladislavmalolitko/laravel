<form action="/updateData/{{$movie_id}}" method="post" enctype="multipart/form-data">
    @csrf
    @if($movie)
        <div class="card bg-light align-items-start">
            <article class="card-body mx-auto" style="max-width: 1000px;">
                <div class="row">
                    <div class="col-5">
                        <div class="row ">
                            <div class="col text-center">
                                <img class="film-img" id="img" src="{{$movie['img']}}" alt="{{$movie['title']}}"/>
                                @error('img')
                                <div class="panel alert-danger">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>
                            <input type="hidden" id="img" name="img" value="{{$movie['img']}}"/>
                            <div class="w-100"></div>
                            <div class="col text-center">

                                <section style="padding-top: 30px; padding-bottom: 20px;">
                                    <div class="card">
                                        <div class="card-header">
                                            Drag and drop the file or click on the form to upload
                                        </div>
                                        <div class="card-body">
                                            <input class="dropzone dz-clickable text-center" name="img" type="file"/>
                                        </div>
                                    </div>

                                </section>
                                <div class="col">
                                    <button type="button" name="img" value="{{$movie['href']}}"
                                            class="refresh btn btn-secondary btn-sm">
                                        Refresh
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-7">
                        <div class="form-group" style="max-width: 500px;">
                            <h5>Title:</h5>
                            <div class="row">
                                <div class="col-5">
                                    <input name="title" type="text" value="{{$movie['title']}}"/>
                                    @error('title')
                                    <div class="panel alert-danger">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>

                                <div class="col">
                                    <button type="button" name="title" value="{{$movie['href']}}"
                                            class="refresh btn btn-secondary btn-sm">
                                        Refresh
                                    </button>
                                </div>
                            </div>
                            <h5>Year:</h5>
                            <div class="row">
                                <div class="col-5">
                                    <input name="year" type="text" value="{{$movie['year']}}"/>
                                    @error('year')
                                    <div class="panel alert-danger">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                                <div class="col">
                                    <button type="button" name="year" value="{{$movie['href']}}"
                                            class="refresh btn btn-secondary btn-sm">
                                        Refresh
                                    </button>
                                </div>
                            </div>

                            <h5>Origin title</h5>
                            <div class="row">
                                <div class="col-5">
                                    <input name="origin_title" type="text" value="{{$movie['origin_title']}}"/>
                                    @error('origin_title')
                                    <div class="panel alert-danger">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                                <div class="col-7">
                                    <button type="button" name="origin_title" value="{{$movie['href']}}"
                                            class="refresh btn btn-secondary btn-sm">
                                        Refresh
                                    </button>
                                </div>
                            </div>
                            @if (!empty($categories))
                                <h5>Category:</h5>
                                <div class="row">
                                    <div class="col-5">
                                        <select class="form-select form-select-sm"
                                                aria-label=".form-select-sm example"
                                                name="category_id">
                                            @foreach ($categories as $category)
                                                <option
                                                    @if ($category['id'] == $movie['category_id'])
                                                    selected
                                                    @endif
                                                    value="{{$category['id']}}">{{$category['name']}}
                                                </option>
                                            @endforeach
                                        </select>
                                        @error('category_id')
                                        <div class="panel alert-danger">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="col-7">
                                        <button type="button" name="category" value="{{$movie['href']}}"
                                                class="refresh btn btn-secondary btn-sm"> Refresh
                                        </button>
                                    </div>
                                </div>
                            @endif

                            <h5>Rating: </h5>
                            <div class="row">
                                <div class="col-5">
                                    <input name="rating" type="text" value="{{$movie['rating']}}"/>
                                    @error('rating')
                                    <div class="panel alert-danger">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                                <div class="col-7">
                                    <button type="button" name="rating" value="{{$movie['href']}}"
                                            class="refresh btn btn-secondary btn-sm">
                                        Refresh
                                    </button>
                                </div>
                            </div>

                            <h5>Runtime: </h5>
                            <div class="row">
                                <div class="col-5">
                                    <input name="runtime" type="text" value="{{$movie['runtime']}}"/>
                                    @error('runtime')
                                    <div class="panel alert-danger">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                                <div class="col-7">
                                    <button type="button" name="runtime" value="{{$movie['href']}}"
                                            class="refresh btn btn-secondary btn-sm">
                                        Refresh
                                    </button>
                                </div>
                            </div>

                            <h5>Storyline: </h5>
                            <div class="row">
                                <div class="col-11">
                                    <textarea name="storyline" rows="5" cols="60">{{$movie['storyline']}}</textarea>
                                    @error('storyline')
                                    <div class="panel alert-danger">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                                <div class="col-1 text-center">
                                    <button type="button" name="storyline" value="{{$movie['href']}}"
                                            class="refresh btn btn-secondary btn-sm">
                                        Refresh
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </article>

            <div class="form-group container text-center">
                <button type="button" name="refreshAll" value="{{$movie['href']}}" class="refresh btn btn-info">
                    Refresh
                </button>
                <button type="submit" name="save" class="btn btn-success">Save</button>
            </div>
        </div>


    @else
        <div class="container">
            <div class="row justify-content-center">
                <h1>No info about movie</h1>
            </div>
        </div>
    @endif
</form>
