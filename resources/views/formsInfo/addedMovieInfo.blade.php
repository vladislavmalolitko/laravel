@if($movie)
    @if($editStatus == 'edit')
        <div class="alert alert-success text-center alert-dismissible fade show" role="alert">
            <h6 class="alert-heading">Data edited successfully</h6>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @elseif($editStatus == 'add')
        <div class="alert alert-success text-center alert-dismissible fade show" role="alert">
            <h6 class="alert-heading">Data added successfully</h6>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="card bg-light align-items-start">
        <article class="card-body mx-auto" style="max-width: 1000px;">
            <div class="row">

                <div class="col-12 text-center">
                    <h1 class="display-4">{{$movie['title']}} ({{$movie['year']}})</h1><br>
                </div>

                <div class="col-5">
                    <div class="row ">
                        <div class="col text-center">
                            <img id="img" src="{{$movie['img']}}" alt="{{$movie['img']}}">
                        </div>
                    </div>
                </div>

                <div class="col-7">
                    <div class="form-group" style="max-width: 500px;">
                        <h5>Origin title</h5>
                        <p>{{$movie['origin_title']}}</p>

                        <h5>Category:</h5>
                        @foreach ($categories as $category)
                            @if ($category['id'] == $movie['category_id'])
                                <p> {{$category['name']}}</p>
                            @endif
                        @endforeach

                        <h5>Rating: </h5>
                        <p>{{$movie['rating']}}</p>

                        <h5>Runtime: </h5>
                        <p>{{$movie['runtime']}}</p>

                        <h5>Storyline: </h5>
                        <p>{{$movie['storyline']}}</p>
                    </div>
                </div>


            </div>
        </article>

        <div class="form-group container text-center">
            <a href="/edit/{{$movie_id}}" class="btn btn-primary">Edit</a>
        </div>
    </div>


@else
    <div class="container">
        <div class="row justify-content-center">
            <h1>No info about movie</h1>
        </div>
    </div>
@endif
