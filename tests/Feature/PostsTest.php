<?php

namespace Tests\Feature;

use App\Models\Movies;
use App\Models\User;
use Tests\TestCase;

class PostsTest extends TestCase
{

    public function test_show()
    {
        $this->seed();
        $user = User::factory()->create();
        $response = $this->actingAs($user)
            ->withSession(['banned' => false])
            ->get('/show/1');

        $response->assertStatus(200);
    }


//    public function test_basic_example()
//    {
//        $this->visit('/search')
//            ->type('peaky', 'title')
//            ->click('nameMovie')
//            ->seePageIs('/preview');
//    }

    public function test_update_movie()
    {
        $user = User::factory()->create();
        $this->actingAs($user)
            ->withSession(['banned' => false])
            ->put('/updateData/1', [
                'title' => 'fyn',
                'img' => 'rvt.jpeg',
                'category_id' => 1,
                'storyline' => 'drtntrtrnst',
                'rating' => 3.2,
                'origin_title' => 'bfdbfd',
                'runtime' => '1h 25min',
                'year' => '1999'
            ]);
        $response = $this->actingAs($user)
            ->withSession(['banned' => false])
            ->get('/show/1')
            ->assertStatus(200);
    }
    public function test_create_movie()
    {
        $user = User::factory()->create();
        $this->actingAs($user)
            ->withSession(['banned' => false])
            ->post('/checkAction', [
                'title' => 'kiki',
                'img' => 'rvt.jpeg',
                'category_id' => 1,
                'storyline' => 'drtntrtrnst',
                'rating' => 3.2,
                'origin_title' => 'bfdbfd',
                'runtime' => '1h 25min',
                'year' => '1999'
            ]);
        $lastMovie = Movies::all()->last();
        $response = $this->actingAs($user)
            ->withSession(['banned' => false])
            ->get('/show/'.$lastMovie->id)
            ->assertStatus(200);
    }

}
