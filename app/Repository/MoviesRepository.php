<?php

namespace App\Repository;

use App\Interfaces\MoviesRepositoryInterface;
use App\Models\Movies;

class MoviesRepository extends BaseRepository implements MoviesRepositoryInterface
{
    public function __construct(Movies $model)
    {
        parent::__construct($model);
    }

    public function getAllMovies(int $id)
    {
        return $this->model->where('user_id', $id);
    }

    public function getMoviesByCategory(int $id, int $categoryId)
    {
        return $this->model->where('user_id', $id)->where('category_id', $categoryId);
    }

    public function getMovieByLink(int $userId, string $link)
    {
//        dd($this->model->where('user_id', $userId)->where('href', $link));
        return $this->model->where('user_id', $userId)->where('href', $link);
    }

    public function getSearchedMovie($userId, $title)
    {
        return $this->model->where('user_id', $userId)->where('origin_title', 'LIKE',
            '%' . $title . '%')->get();
    }
}

