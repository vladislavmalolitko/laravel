<?php

namespace App\Repository;

use App\Interfaces\EloquentRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

abstract class BaseRepository implements EloquentRepositoryInterface
{
    public $sortBy = 'id';
    public $sortOrder = 'desc';
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function all(): Collection
    {
        return $this->model->all();
    }

    public function create(array $attributes): Model
    {
        return $this->model->create($attributes);
    }

    public function update(int $id, array $data)
    {
        $query = $this->model->where('id', $id);
        return $query->update($data);
    }

    public function destroy(int $id): bool
    {
        $this->model->destroy($id);
        return true;
    }

    public function find($id): ?Model
    {
        return $this->model->find($id);
    }

    public function getModel(): Model
    {
        return $this->model;
    }

    public function setModel(Model $model)
    {
        $this->model = $model;
        return true;
    }
}
