<?php

namespace App\Services;

use App\Repository\CategoryRepository;

class CategoryService extends BaseService
{
    public function __construct(CategoryRepository $repo)
    {
        $this->repo = $repo;
    }
}
