<?php

namespace App\Services;

use App\Services\BaseService;
use App\Repository\UserRepository;

class UserService extends BaseService
{
    public function __construct(UserRepository $repo)
    {
        $this->repo = $repo;
    }
    public function getUserId()
    {
        return $this->repo->getUserId();
    }
}