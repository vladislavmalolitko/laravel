<?php

namespace App\Services;

use App\Repository\MoviesRepository;
use App\Services\BaseService;

class MoviesService extends BaseService
{
    public function __construct(MoviesRepository $repo)
    {
        $this->repo = $repo;
    }

    public function getAllMovies(int $userId)
    {
        return $this->repo->getAllMovies($userId);
    }

    public function getMoviesByCategory(int $userId, int $categoryId)
    {
        return $this->repo->getMoviesByCategory($userId, $categoryId);
    }

    public function getMovieInfo(int $movieId)
    {
        $movieInfo = $this->repo->find($movieId);
        if (!empty($movieInfo)) {
            return $movieInfo->toArray();
        }
    }

    public function addMovie($request)
    {
        return $this->repo->create($request);
    }

    public function findMovieByLink($userId, $link)
    {
        return $this->repo->getMovieByLink($userId, $link)->first();
    }

    public function searchInDb($userId, $title)
    {
        return $this->repo->getSearchedMovie($userId, $title);
    }
}
