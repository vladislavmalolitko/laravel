<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class BaseService
{
    public $repo;

    public function all(): Collection
    {
        return $this->repo->all();
    }

    public function create(array $attributes): Model
    {
        return $this->repo->create($attributes);
    }

    public function update(int $id, array $data)
    {
        return $this->repo->update($id, $data);
    }

    public function destroy(int $id): bool
    {
        $this->repo->destroy($id);
        return true;
    }

    public function find($id)
    {
        return $this->repo->find($id);
    }

}
