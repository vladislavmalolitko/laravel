<?php

namespace App\Services;

use App\Services\BaseService;
use Symfony\Component\DomCrawler\Crawler;

class ParseMovieService extends BaseService
{
    public function getTableResult(string $title)
    {
        $html = file_get_contents('https://www.imdb.com/find?q=' . urlencode($title) . '&ref_=nv_sr_sm');
        $explode = explode('<div id="main">', $html);
        if (!empty($explode) && isset($explode[1])) {
            $newHtml = $explode[1];
            if (!empty($newHtml)) {
                $crawler = new Crawler($newHtml);
                $table = $crawler->filter('div.findSection')->eq(0)->filter('table.findList > tr');
                return $this->getResults($table);
            }
        }
    }

    public function getResults($table)
    {
        return $table->each(
            function (Crawler $node, $i) {
                return
                    [
                        "title" => $this->getTitle($node),
                        "link" => $this->getLink($node),
                        "img" => $this->getImage($node)
                    ];
            }
        );
    }

    public function getTitle($node): string
    {
        try {
            return $node->filter('td.result_text > a')->html();
        } catch (\Exception $e) {
            return '';
        }
    }

    public function getLink($node): string
    {
        try {
            return $node->filter('td.result_text > a')->attr('href');
        } catch (\Exception $e) {
            return '';
        }
    }

    public function getImage($node): string
    {
        try {
            return $node->filter('td.primary_photo > a > img')->attr('src');
        } catch (\Exception $e) {
            return '';
        }
    }

    public function getAllMovieInfo($link)
    {
        $crawler = $this->getCrawler($link);
        if ($crawler) {
            return $this->getAll($crawler);
        }
        return false;
    }


    public function getCrawler($link)
    {
        $movieLink = 'https://www.imdb.com/' . $link;
        $html = file_get_contents($movieLink);

        if ($html) {
            return new Crawler($html);
        }
        return false;
    }

    public function getAll(Crawler $crawler): array
    {
        return [
            'img' => $this->getImg($crawler),
            'title' => $this->getNewTitle($crawler),
            'origin_title' => $this->getOriginTitle($crawler),
            'year' => $this->getYear($crawler),
            'rating' => $this->getRating($crawler),
            'runtime' => $this->getRuntime($crawler),
            'storyline' => $this->getStoryline($crawler),
            'category_id' => $this->getCategory($crawler)
        ];
    }

    public function getImg(Crawler $crawler): ?string
    {
        try {
            return $crawler->filter('div[data-testid="hero-media__poster"] > div')->eq(0)->filter('img')->attr('src');
        } catch (\Exception $e) {
            return '';
        }
    }

    public static function getNewTitle(Crawler $crawler): string
    {
        try {
            return trim($crawler->filter('h1[data-testid="hero-title-block__title"]')->html());
        } catch (\Exception $e) {
            return '';
        }
    }

    public function getOriginTitle(Crawler $crawler): string
    {
        try {
            $originalTitle = trim($crawler->filter('div[data-testid="hero-title-block__original-title"]')->html());
            $originalTitle = explode(":", $originalTitle);
            return trim($originalTitle[1]);
        } catch (\Exception $e) {
            return '';
        }
    }

    public function getYear(Crawler $crawler): string
    {
        try {
            return ($crawler->filter('span[class="TitleBlockMetaData__ListItemText-sc-12ein40-2 jedhex"]')->html());
        } catch (\Exception $e) {
            return '';
        }
    }

    public function getRating(Crawler $crawler): string
    {
        try {
            return trim($crawler->filter('div[data-testid="hero-rating-bar__aggregate-rating__score"] > span')->eq(0)->html());
        } catch (\Exception $e) {
            return '';
        }
    }

    public function getRuntime(Crawler $crawler): string
    {
        try {
            $runtime = trim($crawler->filter('div.TitleBlock__TitleMetaDataContainer-sc-1nlhx7j-2 > ul > li ')->last()->text());
            if ((!strstr($runtime, 'h')) && (!strstr($runtime, 'min'))) {
                return 'No info';
            }
            return $runtime;
        } catch (\Exception $e) {
            return '';
        }
    }

    public function getStoryline(Crawler $crawler): string
    {
        try {
            return trim($crawler->filter('div[data-testid="storyline-plot-summary"] > div>div')->text());
        } catch (\Exception $e) {
            return '';
        }
    }

    public function getCategory(Crawler $crawler)
    {
        try {
            $category = trim($crawler->filter('ul[data-testid="hero-title-block__metadata"]>li')->eq(0)->text());
            if (strstr($category, 'TV Series')) {
                return 3;
            } else {
                $category = false;
            }
        } catch (\Exception $e) {
            $category = false;
        }
        if (!$category) {
            try {
                $category = trim($crawler->filter('li[data-testid="storyline-genres"]>div>ul>li')->eq(0)->text());
                if (strstr($category, 'Animation')) {
                    return 2;
                } else {
                    $category = false;
                }
            } catch (\Exception $e) {
                $category = false;
            }
            if (!$category) {
                return 1;
            }
        }
    }


}
