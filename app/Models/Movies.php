<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movies extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'title',
        'href',
        'img',
        'category_id',
        'user_id',
        'storyline',
        'rating',
        'origin_title',
        'runtime',
        'year',
    ];




    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'movies';

}
