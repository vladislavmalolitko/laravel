<?php

namespace App\Http\Controllers;

use App\Http\Requests\MoviePostRequest;
use App\Services\CategoryService;
use App\Services\MoviesService;
use App\Services\ParseMovieService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MovieController extends Controller
{
    private $moviesService;
    private $categoryService;
    private $userService;
    private $parseMovieService;

    public function __construct(
        MoviesService $moviesService,
        CategoryService $categoryService,
        UserService $userService,
        ParseMovieService $parseMovieService
    ) {
        $this->moviesService = $moviesService;
        $this->categoryService = $categoryService;
        $this->userService = $userService;
        $this->parseMovieService = $parseMovieService;
    }

    public function showMoviesByCategory(Request $request, $categoryId)
    {
        $movies = $this->moviesService->getMoviesByCategory(Auth::id(), $categoryId);
        $categories = $this->categoryService->all();
        return view('pages.home_page', ['movies' => $movies->paginate(4), 'categories' => $categories]);
    }

    public function searchMovie(Request $request)
    {
        $result = $this->parseMovieService->getTableResult($request->get('keyword'));
        return response()->json($result);
    }

    public function deleteMovie($movieId)
    {
        $this->moviesService->destroy($movieId);
        return $this->showMovies();
    }

    public function showMovies()
    {
        $movies = $this->moviesService->getAllMovies(Auth::id());
        $categories = $this->categoryService->all();
        return view('pages.home_page', ['movies' => $movies->paginate(4), 'categories' => $categories]);
    }

    public function checkMovieInDb(Request $request)
    {
        $movie = $this->moviesService->findMovieByLink(Auth::id(), $request->input('link'));
        if ($movie) {
            return redirect()->route('show', [$movie]);
        }
        return $this->parseMovieInfo($request->input('link'));
    }

    public function parseMovieInfo(string $link)
    {
        $categories = $this->categoryService->all();
        $movieInfo = $this->parseMovieService->getAllMovieInfo($link);
        return view('pages.info_movie', [
            'movie' => $movieInfo,
            'categories' => $categories,
            'status' => 'noAddedMovieInfo',
            'link' => $link,
            'user_id' => Auth::id()
        ]);
    }

    public function show($movieId)
    {
        $movieInfo = $this->moviesService->getMovieInfo($movieId);
        $categories = $this->categoryService->all();
        return view('pages.info_movie',
            [
                'movie' => $movieInfo,
                'categories' => $categories,
                'status' => 'addedMovieInfo',
                'movie_id' => $movieId,
                'editStatus' => session('status')
            ]);
    }

    public function checkAction(Request $request)
    {
        $addedMovie = $this->moviesService->addMovie($request->all());
        return redirect()->route('show', $addedMovie)->with('status', 'add');
    }

    public function editMovie(int $movieId)
    {
        $movieInfo = $this->moviesService->getMovieInfo($movieId);
        $categories = $this->categoryService->all();
        return view('pages.info_movie',
            [
                'movie' => $movieInfo,
                'categories' => $categories,
                'status' => 'editAddedMovieInfo',
                'user_id' => Auth::id(),
                'movie_id' => $movieId
            ]);
    }

    public function editAddedMovieInfo(MoviePostRequest $request, $movieId)
    {
        $validated = $request->validated();
        if ($request->hasFile('img')) {
            $path = $request->file('img')->store('movieImages', 'public');
            $src = '/storage/' . $path;
            $validated['img'] = $src;
        } else {
            $validated['img'] = $request->get('img');
        }
        $this->moviesService->update($movieId, $validated);
        return redirect()->route('show', $movieId)->with('status', 'edit');
    }

    public function refreshData(Request $request)
    {
        $crawler = $this->parseMovieService->getCrawler($request->get('link'));
        switch ($request->get('name')) {
            case 'title':
                $title = $this->parseMovieService->getNewTitle($crawler);
                return response()->json($title);
            case 'year':
                $year = $this->parseMovieService->getYear($crawler);
                return response()->json($year);
            case 'category':
                $category = $this->parseMovieService->getCategory($crawler);
                return response()->json($category);
            case 'origin_title':
                $originTitle = $this->parseMovieService->getOriginTitle($crawler);
                return response()->json($originTitle);
            case 'rating':
                $rating = $this->parseMovieService->getRating($crawler);
                return response()->json($rating);
            case 'runtime':
                $runtime = $this->parseMovieService->getRuntime($crawler);
                return response()->json($runtime);
            case 'storyline':
                $storyline = $this->parseMovieService->getStoryline($crawler);
                return response()->json($storyline);
            case 'img':
                $img = $this->parseMovieService->getImg($crawler);
                return response()->json($img);
            case 'refreshAll':
                $refreshAll = $this->parseMovieService->getAll($crawler);
                return response()->json($refreshAll);
        }
    }

    public function autocompleteInDb(Request $request)
    {
        $userId = Auth::id();
        $query = $request->get('keyword');
        $result = $this->moviesService->searchInDb($userId, $query);
        return response()->json($result);
    }
}
