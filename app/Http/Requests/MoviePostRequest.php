<?php

namespace App\Http\Requests;

use App\Models\Movies;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class MoviePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

//    public function withValidator($validator)
//    {
//        $validator->after(function ($validator) {
//            if ($validator->invalid()) { // fail with image
//                return Validator::make($this->input(), [
//                    'img' => ['sometimes','url']
//                ]);
//            }
////            dd($validator->errors(), 3);
////            if ($this->somethingElseIsInvalid()) {
////                $validator->errors()->add('field', 'Something is wrong with this field!');
////            }
//        });
//    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => ['required', 'string', 'min:1', 'max:255'],
            'year' => ['required', 'string', 'min:4', 'max:20'],
//            'img' => ['sometimes', 'image', 'max:2048', 'dimensions:,max_width=195,max_height=285'],
//            'img' => ['sometimes','url'],
            'img.*' => ['sometimes', 'mimes: jpg, jpeg, png'],
            'origin_title' => ['required', 'string', 'max:255'],
            'category_id' => ['required', 'int', 'min:1'],
            'rating' => ['required', 'numeric', 'between:0, 10.0'],
            'runtime' => ['required', 'string', 'max:10'],
            'storyline' => ['required', 'string', 'max:1000']
        ];

        return $rules;
    }
}
