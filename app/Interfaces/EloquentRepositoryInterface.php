<?php
namespace App\Interfaces;

use Illuminate\Database\Eloquent\Model;

/**
 * Interface EloquentRepositoryInterface
 * @package App\Interfaces
 */
interface EloquentRepositoryInterface
{
    public function all();
 
    public function create(array $attributes);

    public function update(int $id, array $data);
    
    public function destroy(int $id);
    
    public function find($id);
}
