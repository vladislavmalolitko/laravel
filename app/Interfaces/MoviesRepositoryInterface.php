<?php
namespace App\Interfaces;

use App\Models\Movies;
use Illuminate\Support\Collection;

interface MoviesRepositoryInterface
{
    public function getAllMovies(int $id);
    public function getMoviesByCategory(int $id, int $categoryId);
    public function getMovieByLink(int $userId, string $link);
    public function getSearchedMovie(int $userId, string $title);
}
