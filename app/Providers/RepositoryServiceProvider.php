<?php

namespace App\Providers;

use App\Interfaces\EloquentRepositoryInterface;
use App\Interfaces\MoviesRepositoryInterface;
use App\Interfaces\CategoryRepositoryInterface;
use App\Interfaces\UserRepositoryInterface;
use App\Repository\MoviesRepository;
use App\Repository\CategoryRepository;
use App\Repository\UserRepository;
use App\Repository\BaseRepository;
use http\Client\Curl\User;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(EloquentRepositoryInterface::class, BaseRepository::class);
        $this->app->bind(MoviesRepositoryInterface::class, MoviesRepository::class);
        $this->app->bind(CategoryRepositoryInterface::class, CategoryRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
