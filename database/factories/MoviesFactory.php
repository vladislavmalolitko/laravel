<?php

namespace Database\Factories;

use App\Models\Movies;
use Illuminate\Database\Eloquent\Factories\Factory;

class MoviesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Movies::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
                    'title' => $this->faker->name(),
                    'href' => $this->faker->url(),
                    'img' => $this->faker->image(),
                    'category_id' => rand(1, 3),
                    'user_id' => 1,
                    'storyline' => $this->faker->text(),
                    'rating' => rand(1.0, 9.9),
                    'origin_title' => $this->faker->name(),
                    'runtime' => '1h 25min',
                    'year' => $this->faker->year(),
        ];
    }
}
